import React, { useState, useEffect } from "react";
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    FlatList,
    ToastAndroid
} from "react-native";
import { Asset } from "expo-asset";
import Constants from "expo-constants";
import AppLoading from "expo-app-loading";
import * as Clipboard from "expo-clipboard";
import * as SQLite from "expo-sqlite";
import * as FileSystem from "expo-file-system";

export default function App() {
    const [isReady, setIsReady] = useState(false);
    const [loading, setLoading] = useState(false);
    const [items, setItems] = useState([]);
    const [query, setQuery] = useState("");
    const [queryResult, setQueryResult] = useState([]);

    /*
     * For whatever reason, opening the database in Items does not work.
     * We need to open it immediately in the .finish method in
     * moveDatabase. Any later calls do not call any callback functions,
     * despite:
     * - the database existing and giving the same readout on inspection.
     * - the database transaction existing, and the executeSql method existing.
     *
     * I have absolutely no idea why this is.
     * I can't find the customOpenDatabase definition in Expo, nor the executeSql
     * definition. And right now I do not care.
     * It is 3am. Stuff it.
     * */

    const startup = () => {
        moveDatabase();
    };

    const openDatabase = () => {
        let db = SQLite.openDatabase("sa-emojis.db");
        db.transaction((tx) => {
            tx.executeSql(
                "SELECT * FROM emojis;",
                [],
                (tx, { rows: { _array: items_array }}) => {
                    console.log(
                        "Loaded " + items_array.length + " items."
                    );
                    setItems(items_array);
                    setQueryResult(items_array);
                },
                (tx, error) => {
                    console.error(error);
                }
            );
        });
    };

    const moveDatabase = async () => {
        try {
            await FileSystem.makeDirectoryAsync(
                `${FileSystem.documentDirectory}/SQLite`,
                {
                    intermediates: true
                }
            );
            const dir = FileSystem.documentDirectory + "SQLite/sa-emojis.db";
            const localDatabase = await FileSystem.getInfoAsync(dir);
            if (!localDatabase.exists) {
                const asset = Asset.fromModule(
                    require("./assets/sa-emojis.db")
                );
                FileSystem.downloadAsync(asset.uri, dir)
                .then(() => {
                    console.log("Download success");
                    openDatabase();
                    setLoading(true);
                })
                .catch((error) => {
                    console.error(error);
                });
            } else {
                console.log("Local database does exist.")
                openDatabase();
                setLoading(true);
            }
        } catch (error) {
            console.error(error);
            setLoading(true);
        }
    };

    if (!loading) {
        return (
            <AppLoading startAsync={startup}
                        onError={(error) => { console.error(error) }}
                        onFinish={() => setIsReady(true)}
            />
        );
    }

    function filterItems() {
        setQueryResult(items.filter((item, index, _) => {
            return item.name.search(query) >= 0 ||
                item.description.search(query) >= 0;
        }));
        // console.warn(
        //     items.filter((item, index, _) => {
        //         return item.name.search(query) >= 0 ||
        //             item.description.search(query) >= 0;
        //     }).map((item) => {
        //         return item.name
        //     })
        // );
    }

    return (
      <View style={styles.container}>
        <View style={styles.flexRow}>
          <TextInput
            onChangeText={(str) => {
                setQuery(str);
            }}
            onSubmitEditing={() => {
                filterItems();
            }}
            placeholder="Find an emoji"
            value={query}
            clearOnFocus={true}
            textAlign="center"
            selectTextOnFocus={true}
            style={{
                fontSize: 16,
                height: 40,
                borderColor: "#000",
                borderRadius: 4,
                borderWidth: 1,
                margin: 8
            }}
          />
        </View>
        <Items items={ queryResult }/>
      </View>
    );

}

function Items({ items }) {
    function onPressItem(url) {
        Clipboard.setString(url);
        ToastAndroid.show("Copied URL to clipboard", ToastAndroid.SHORT);
    }

    const renderItems = ({item}) => (
        <TouchableOpacity
          key={item.name}
          onPress={() => onPressItem && onPressItem(item.url)}
          style={{
              borderColor: "#000",
              borderWidth: 1,
              padding: 8,
              margin: 8,
              justifyContent: "center",
              alignItems: "center",
              width: item.width,
              minWidth: "28%",
          }}>
            <Image
              style={{
                  width: item.width,
                  height: item.height,
              }}
              source={{ uri: item.image }}
            />
            <Text style={{ color: "#000", fontSize: 14, }}>{item.name}</Text>
        </TouchableOpacity>
    );

    return (
        <View style={styles.sectionContainer}>
            <FlatList data={ items }
                      numColumns={3}
                      renderItem={renderItems}
                      extraData={ items }
                      keyExtractor={ item => item.url } />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        flex: 1,
        paddingTop: Constants.statusBarHeight,
    },
    sectionContainer: {
        marginBottom: 16,
        marginHorizontal: 16,
    },
});
